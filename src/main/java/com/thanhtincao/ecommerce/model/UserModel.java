package com.thanhtincao.ecommerce.model;

public record UserModel (String userName, String password) {
  public UserModel(){
    this("defaultValue", "defaultValue");
  }
}
