package com.thanhtincao.ecommerce.util;

import org.springframework.http.*;
import org.springframework.util.MultiValueMap;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.RestTemplate;

public class RestTemplateUtil {

  private RestTemplateUtil() {}

  static RestTemplate restTemplate = new RestTemplate();

  public static <T> T makeRequest(
      String url,
      HttpHeaders headers,
      HttpMethod httpMethod,
      MultiValueMap<String, String> body,
      Class<T> clazz) {
    T response = null;
    var responseEntity = new ResponseEntity<T>(HttpStatus.BAD_REQUEST);
    HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(body, headers);
    try {
      responseEntity = restTemplate.exchange(url, httpMethod, request, clazz);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    if (!ObjectUtils.isEmpty(responseEntity.getBody())
        && responseEntity.getStatusCode() == HttpStatus.OK) {
      response = responseEntity.getBody();
    }
    return response;
  }
}
