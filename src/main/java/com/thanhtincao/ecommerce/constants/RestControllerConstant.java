package com.thanhtincao.ecommerce.constants;

public class RestControllerConstant {
  private RestControllerConstant() {}

  public static final String REGULAR = "/regular";
  public static final String COMMON = "/common";
  public static final String CUSTOMER = "/customer";
}
