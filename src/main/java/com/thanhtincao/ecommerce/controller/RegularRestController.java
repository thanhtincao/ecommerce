package com.thanhtincao.ecommerce.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.time.LocalDate;
import java.time.LocalTime;

import static com.thanhtincao.ecommerce.constants.RestControllerConstant.REGULAR;

@Api
@RestController
@RequestMapping(REGULAR)
public class RegularRestController {

  @ApiIgnore
  @ApiOperation(value = "This method is used to get the author name.")
  @GetMapping("/getAuthor")
  public String getAuthor() {
    return "Umang Budhwar";
  }

  @ApiOperation(value = "This method is used to get the current date.", hidden = true)
  @GetMapping("/getDate")
  public LocalDate getDate() {
    return LocalDate.now();
  }

  @ApiOperation(value = "This method is used to get the current time.")
  @GetMapping("/getTime")
  public LocalTime getTime() {
    return LocalTime.now();
  }

  @GetMapping(path = "/customers")
  public String customers() {
    return "customers";
  }
}
