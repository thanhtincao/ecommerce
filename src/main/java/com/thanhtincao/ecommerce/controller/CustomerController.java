package com.thanhtincao.ecommerce.controller;


import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.security.Principal;

import static com.thanhtincao.ecommerce.constants.RestControllerConstant.CUSTOMER;

@Api
@RestController
@RequestMapping(CUSTOMER)
public class CustomerController {

    @ApiOperation(value = "This method is used to get customer information.")
    @RolesAllowed("user")
    @GetMapping(path = "/user")
    public String user(Principal principal) {
        return principal.toString();
    }

    @ApiOperation(value = "This method is used to get customer information.")
    @RolesAllowed("admin")
    @GetMapping(path = "/admin")
    public String admin(Principal principal) {
        return principal.toString();
    }

    @ApiOperation(value = "This method is used to get customer information.")
    @RolesAllowed({"admin", "user"})
    @GetMapping(path = "/all")
    public String all(Principal principal) {
        return principal.toString();
    }
}
